# -*- coding: utf-8 -*-
"""
    Send a SMS via Twilio from an external list of messages.
"""
import os
import random
import logging

import simplejson
from twilio.rest import Client

CONFIGURATION_FILE_NAME = 'config.json'
QUOTES_FILE_NAME = 'quotes.txt'
EXIT_CODE_SUCCESS = 0
EXIT_CODE_CONFIGURATION_FILE = -1
EXIT_CODE_QUOTES_FILE = -2
EXIT_CODE_NO_QUOTE = -3
EXIT_CODE_SENDING = -4

logger = None


def check_prerequisites():
    """
    Check prerequisites needed for the script. Let it crash if not all prerequisites are met.

    :return: Configuration and filename of quotes
    """
    current_path = os.path.dirname(os.path.abspath(__file__))
    cfg_filename = os.path.join(current_path, CONFIGURATION_FILE_NAME)
    logger.debug(f'Configuration filename: {cfg_filename}')
    if not os.path.isfile(cfg_filename):
        logger.critical(f'Configuration file {cfg_filename} doesn\'t exist, exiting')
        exit(EXIT_CODE_CONFIGURATION_FILE)
    with open(cfg_filename) as f:
        cfg = simplejson.load(f)
    logger.debug(f'Configuration loaded: {cfg}')
    quotes_filename = os.path.join(current_path, QUOTES_FILE_NAME)
    logger.debug(f'Quotes filename: {quotes_filename}')
    if not os.path.isfile(quotes_filename):
        logger.critical(f'Quotes file {quotes_filename} doesn\'t exit, exiting')
        exit(EXIT_CODE_QUOTES_FILE)
    return cfg, quotes_filename


def pop_quote(quotes_filename):
    """
    Retrieve a random quote and remove it from the file

    :param quotes_filename: Filename of the quotes file to use
    :return: Quote or None if no quote could get retrieved
    """
    logger.debug(f'Reading quotes file {quotes_filename}')
    with open(quotes_filename) as f:
        quotes = f.read().splitlines()
    logger.debug(f'Read {len(quotes)} lines')

    if not quotes:
        logger.warning('No quotes found')
        return None

    logger.debug('Picking random quote')
    picked_quote = random.choice(quotes)
    logger.debug(f'Picked quote "{picked_quote}"')
    logger.debug('Removing picked quote from list')
    quotes.remove(picked_quote)
    logger.debug('Re-writing quotes file')
    with open(quotes_filename, 'w') as f:
        for item in quotes:
            f.write('%s\n' % item)
    logger.debug('Quotes file rewritten')
    return picked_quote


def send_sms(twilio_configuration, receiver, message):
    """
    Send a sms containing the quote using the configuration

    :param twilio_configuration: Twilio configuration to use
    :param receiver: Phone number to receive the quote
    :param message: Message to send
    :return: Flag if sending received, SID of the sent message if
        sending succeeded or the error message received from Twilio
    """
    client = Client(twilio_configuration['account_sid'], twilio_configuration['auth_token'])

    try:
        message = client.messages.create(
            to=receiver,
            from_=twilio_configuration['phone_number_sender'],
            body=message)
        return True, message.sid
    except Exception as exc:
        logger.exception('Sending failed, error: ')
        return False, str(exc)


def run():
    """
    Execute the script.

    :return: None
    """
    configuration, quotes_filename = check_prerequisites()

    logger.debug(f'Calling pop_quote')
    quote = pop_quote(quotes_filename)
    if not quote:
        logger.critical('Couldn\'t retrieve quote, file may be empty, exiting')
        exit(EXIT_CODE_NO_QUOTE)

    logger.debug(f'Retrieved quote: {quote}')
    for receiver in configuration['phone_number_receivers']:
        logger.debug(f'Sending quote to f{receiver}')
        result, result_message = send_sms(configuration['twilio'], receiver, quote)
        if result:
            logger.debug(f'Quote sent, {result_message}')
        else:
            exit(EXIT_CODE_SENDING)
    logger.info('Done')
    exit(EXIT_CODE_SUCCESS)


if __name__ == '__main__':
    logger = logging.getLogger()
    run()

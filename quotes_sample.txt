Steh auf und pack es einfach an!
An unmöglichen Dingen soll man selten verzweifeln, an schweren nie.
Wenn dir das Leben in den Arsch tritt, dann nutze den Schwung, um vorwärts zu kommen.
Suche nicht nach Fehlern, suche nach Lösungen.
Mitleid bekommt man geschenkt, Neid muss man sich verdienen.
Menschen mit einer neuen Idee gelten so lange als Spinner, bis sich die Sache durchgesetzt hat.
Zuerst ignorieren sie dich, dann lachen sie über dich, dann bekämpfen sie dich und dann gewinnst du.
Wenn dir jemand erzählt, deine Idee sei verrückt – höre nicht auf ihn.
Einfach machen.
Einen Vorsprung im Leben hat, wer da anpackt, wo die anderen erst einmal reden.
Wenn Sie auf den Mond zielen, und Sie treffen ihn nicht, landen Sie noch immer bei den Sternen!
Die Kunst ist, einmal mehr aufzustehen, als man umgeworfen wird.
Unsere Fehlschläge sind oft erfolgreicher als unsere Erfolge. –
Wer einen Fehler gemacht hat und ihn nicht korrigiert, begeht einen zweiten.
Du kannst niemals aufhören. Gewinner hören niemals auf und Drückeberger gewinnen niemals.
Ein Optimist findest immer einen Weg. Ein Pessimist findet immer eine Sackgasse.
Alle Träume können wahr werden, wenn wir den Mut haben, ihnen zu folgen.
Phantasie ist wichtiger als Wissen, denn Wissen ist begrenzt.
Wir leben alle unter dem gleichen Himmel, aber wir haben nicht alle den gleichen Horizont.
Die Entfernung ist egal. Was zählt, ist der erste Schritt.
Wer kein Ziel hat, kann auch keins erreichen.
Wenn jemand zu dir sagt „Das geht nicht“, dann denke immer daran: Es sind seine Grenzen, nicht deine!
Gehe deinen Weg und lass die Leute reden.
Du hast drei Möglichkeiten: Aufgeben, nachgeben oder alles geben.
Eine große Flamme entsteht aus einem kleinen Funken.
Wer etwas will, der findet Wege. Wer etwas nicht will, der findet Gründe.
Anstatt deine Träume aufzugeben, gib lieber deine Zweifel auf!
Deine Zeit ist begrenzt, also verschwende sie nicht, um das Leben eines Anderen zu leben.
Wenn die Guten nicht kämpfen, werden die Schlechten siegen.
Die beste Form der Rache ist Erfolg.
Wer kein Ziel hat, kann auch keines erreichen.
Nichts wissen ist keine Schande, wohl aber, nichts lernen wollen.
Ein Problem ist halb gelöst, wenn es klar formuliert ist.
Eine Investition in Wissen bringt noch immer die besten Zinsen.
Erfolg entsteht, wenn man die Ohren spitzt und den Mund schließt.
Niemand, der sein Bestes gegeben hat, hat es später bereut.
Wann immer du sagst: „Das hätte ich auch geschafft“, denke daran, sie haben es getan.
Erfolg ist eine Treppe, keine Tür.
Zuerst fragen sie dich, warum du das machst. Später fragen sie dich, wie du das machst!
Probleme sind Gelegenheiten, zu zeigen, was man kann.
Egal was um Dich herum passiert, nimm es nicht persönlich... Nichts, was andere Leute tun, passiert wegen Dir, sondern wegen ihnen selbst
Mit leerem Kopf nickt es sich leichter.
Erfolg hat drei Buchstaben: TUN!
